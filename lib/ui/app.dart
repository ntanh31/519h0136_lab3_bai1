import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab3_bai1/stream/stream_word.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  StreamWord streamWord = StreamWord();
  List<String> listWord = [];
  String word = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    streamWord.getWords().listen((event) {
      setState(() {
        word = event;
        listWord.add(word);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Stream New Words",
      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        body:ListView.builder(
            itemCount: listWord.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: EdgeInsets.only(top: 10),
                height: 100,
                color: (index) % 2 == 0 ? Colors.green : Colors.teal,
                child: Center(child: Text(listWord[index], style: TextStyle(fontSize: 20),)),
              );
            }
        )
      ),
    );
  }

}
