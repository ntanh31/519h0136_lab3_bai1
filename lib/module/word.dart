import 'dart:convert';
class Word{
  final String word;
  final num difficulty;
  final String definition;

  const Word({
    required this.word,
    required this.difficulty,
    required this.definition,
  });

  factory Word.fromJson(json) {
    return Word(
      word: json['word'],
      difficulty: json['difficulty'],
      definition: json['definition'],
    );
  }

  @override
  String toString() {
    return 'Word: $word; difficulty: $difficulty; definition: $definition';
  }
}