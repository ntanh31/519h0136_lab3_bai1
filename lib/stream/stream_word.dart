import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:lab3_bai1/module/word.dart';

class StreamWord{
  Stream<String> getWords() async* {
    final response = await http.get(Uri.parse('https://raw.githubusercontent.com/fvrtrp/wordsta/json/baron-334.json'));

    Map<String, dynamic> map = json.decode(response.body);
    List<dynamic> data = map["words"];

    List<String> words = [];

    for(var i in data){
      Word word = Word.fromJson(i);
      words.add(word.toString());
    }

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % words.length;
      return words[index];
    });
  }
}